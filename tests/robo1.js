const mqtt = require('mqtt');

const client = mqtt.connect('mqtt://12.240.100.100', {
	username: 'mosquitto',
	password: 'mosquitto',
	clientId: 'ROBO1_MOCK'
});

const ID_ROBO = 'robo/1';
const ID_APPINVENTOR = 'appinventor/1';

client.on('connect', () => {
	
	client.subscribe(ID_ROBO, (err) => {
		if (!err) {
			console.log("CONNECTED!");
		}
		else {
			console.log("NOT CONNECT!");
		}
	});
});

client.on('message', (topic, message) => {
	console.log(ID_ROBO, message.toString());
	client.publish(ID_APPINVENTOR, message);
});