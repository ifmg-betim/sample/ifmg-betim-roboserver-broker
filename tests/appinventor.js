const mqtt = require('mqtt');

const client = mqtt.connect('mqtt://12.240.100.100', {
	username: 'mosquitto',
	password: 'mosquitto'
});

const ID_ROBO = 'robo/1';

client.on('connect', () => {
	client.publish(ID_ROBO, "F99X00F99");
	client.end();
});
