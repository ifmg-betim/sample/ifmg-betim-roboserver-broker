#include <ESP8266WiFi.h>

#define MQTT_SOCKET_TIMEOUT 30

#include <PubSubClient.h>

//defines - mapeamento de pinos do NodeMCU
#define D0    16
#define D1    5
#define D2    4
#define D3    0
#define D4    2
#define D5    14
#define D6    12
#define D7    13
#define D8    15
#define D9    3
#define D10   1

// WIFI
const char * SSID = "wrobos";
const char * PASSWORD = "ifmg1234567890";

// MQTT
const char * BROKER_MQTT = "12.240.100.100";
int          BROKER_PORT = 1883;
#define      ID_MQTT  "CARRO_01"

#define      TOPICO_SUBSCRIBE "robo/1"
#define      TOPICO_PUBLISH   "appinventor/1"

#define      LETRA_FRENTE   'A'
#define      LETRA_RE       'V'

#define      MIN 966
#define      MAX 1023

//Variáveis e objetos globais
WiFiClient espClient;
PubSubClient MQTT(espClient);

long lastnow = 0;

void setnow() {
  lastnow = millis();
}

int getnow(long t) {

  long now = millis();
  
  if ((now - lastnow) > t) {
    lastnow = now;
    return 1;
  }
  
  return 0;
}

String BytetoString(byte * payload, unsigned int length) {

    String msg = "";

    for (int i = 0; i < length; i++)
    {
        msg += (char)payload[i];
    }

    return msg;
}

String ChartoString(char * payload, unsigned int length) {

    String msg = "";

    for (int i = 0; i < length; i++)
    {
        msg += (char)payload[i];
    }

    return msg;
}

void setupPinos (void)
{
  /*
    pinMode(D0, OUTPUT);
    digitalWrite(D0, HIGH);
  */

  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);

  digitalWrite(D1, 0);
  digitalWrite(D2, 0);

  digitalWrite(D3, 0);
  digitalWrite(D4, 0);
}


int connectWifi ()
{
    if (WiFi.status() == WL_CONNECTED) {
        return 1;
    }
    else {

        WiFi.hostname(ID_MQTT);
        WiFi.begin(SSID, PASSWORD);
        
        while (WiFi.status() != WL_CONNECTED)
        {
            delay(500);
            Serial.println("###########################");
        }

        Serial.println(" ");
        Serial.print("Conectado com sucesso na rede: ");
        Serial.print(SSID);

        Serial.println(" ");
        Serial.print("IP obtido: ");
        Serial.print(WiFi.localIP());
        Serial.println(" ");

        return 1;
    }

    return 0;
}

int connectMQTT () {

    if (MQTT.connected())
    {
        return 1;
    } 
    else {
        
        if (!MQTT.connected() && (WiFi.status() == WL_CONNECTED))
        {
            Serial.print("* Tentando se conectar ao Broker MQTT: "); Serial.print(BROKER_MQTT);
            Serial.println(" ");
            
            while (!MQTT.connect(ID_MQTT, "mosquitto", "mosquitto"))
            {
                Serial.println("Falha ao reconectar no broker. Havera nova tentatica de conexao em 2s");
                
                Serial.println("STATE: ");
                Serial.print(MQTT.state());
                Serial.println(" ");

                delay(2000);
            }

            Serial.println("Conectado com sucesso ao broker MQTT!");
            MQTT.subscribe(TOPICO_SUBSCRIBE);
            
            return 1;
        }
    }

    return 0;
}

void command(char lE, int vE, char lD, int vD) {

    if (vE > 0 && lE == LETRA_FRENTE) {
      digitalWrite(D3, 0);
    }
    
    if (vE > 0 && lE == LETRA_RE) {
      digitalWrite(D3, 1);
    }
    
    if (vE > 0) {
      analogWrite(D1, map(vE, 1, 99, MIN, MAX));
    }
    else {
      analogWrite(D1, 0);
    }

    /**-----------------------------------------------**/

    if (vD > 0 && lD == LETRA_FRENTE) {
      digitalWrite(D4, 0);
    }
    
    if (vD > 0 && lD == LETRA_RE) {
      digitalWrite(D4, 1);
    }
    
    if (vD > 0) {
      analogWrite(D2, map(vD, 1, 99, MIN, MAX));
    }
    else {
      analogWrite(D2, 0);
    }
}

void mqtt_callback(char* topic, byte * payload, unsigned int length)
{
    String message = BytetoString(payload, length);
    char out[100];

    Serial.println(length);
    sprintf(out, "%s", message.c_str());
    
    if (length == 6) {
      
      int i = 0;
  
      i = 0;
      int    valorEsquerdo = message.substring(i, i + 2).toInt();
      i = 2;
      char letraEsquerdo = message.charAt(i);
      
      i = 3;
      char letraDireito = message.charAt(i);
      i = 4;
      int    valorDireito = message.substring(i, i + 2).toInt();
      
      command(letraEsquerdo, valorEsquerdo, letraDireito, valorDireito);
      
      sprintf(out, "COMMAND => %i[%c]---[%c]%i ", valorEsquerdo, letraEsquerdo, letraDireito, valorDireito);  
    }
    
    Serial.println(out);
    MQTT.publish(TOPICO_PUBLISH, out);
    
    setnow();
}

void setup()
{
    ESP.eraseConfig();
    WiFi.mode(WIFI_STA);

    setupPinos();
    Serial.begin(115200);

    Serial.println("------Conexao WI-FI------");
    Serial.print("Conectando-se na rede: ");
    Serial.println(SSID);
    Serial.println("Aguarde");
    
    MQTT.setServer(BROKER_MQTT, BROKER_PORT);
    MQTT.setCallback(mqtt_callback);

    if (connectWifi()) {
      
      delay(500);
      
      if (connectMQTT()) {
        Serial.println("DONE.");
      }
    }
}

void reconnect()
{
  if(connectWifi()) {
    connectMQTT();
  }
}

void loop() {
    
    if (MQTT.connected()) {
      if (MQTT.loop()) {
        
        if (getnow(2000)) {
          command(LETRA_FRENTE, 0, LETRA_RE, 0);
        }
        
      }
    }
    else {
      reconnect();
      delay(500);
    }
}
