const express = require('express')
const app = express()

const mqtt = require('mqtt');

const ID_ROBO = 'robo/';
const ID_APPINVENTOR = 'appinventor/';

const ServerMQTT = (id) => {

	let client = mqtt.connect('mqtt://roboserver.roboslan.betim.ifmg.com.br', {
		username: 'mosquitto',
		password: 'mosquitto',
		clientId: 'HTTP_SERVER'
	});

	client.on('connect', () => {

		client.subscribe(ID_APPINVENTOR + id, (err) => {
			if (!err) {
				console.log("CONNECTED!");
			}
			else {
				console.log("NOT CONNECT!");
			}
		});
	});

	client.on('message', (topic, message) => {
		console.log('FEEDBACK [' + ID_ROBO + ']: ', message.toString());
	});

	return { 
		client: client, 
		publish : (command) => {
			client.publish(ID_ROBO + id, command);
		}
	};
};

const Robo1 = ServerMQTT(1);

app.get('/', function(req, res) {
	res.send('Service listed! \n /robo/1/command/99AA99 \n /robo/1/command/99VV99 ');
});

app.get('/robo/:robo/command/:command', function(req, res) {

	console.log(req.params);

	let ID_ROBO = req.params['robo'];
	let command = req.params['command'];

	if (ID_ROBO == 1) {
		Robo1.publish(command);
	}

	res.send(command);
});

const PORT = 8888;

app.listen(PORT, function() {
	console.log('App listening on port ' + PORT + '!');
});