#!/bin/bash

if [ ! -f /mosquitto/config/pwfile ]; then
	touch /mosquitto/config/pwfile
	mosquitto_passwd -b /mosquitto/config/pwfile mosquitto mosquitto
if